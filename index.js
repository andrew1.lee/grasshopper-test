const connectMongo = require("./connect");
const fs = require("fs");
const Database = require("better-sqlite3");
const { S3Client, GetObjectCommand } = require("@aws-sdk/client-s3");

const FILE_NAME = "logos.db";
const BASE_URL =
  "http://supair.dishanywhere.com/supair-images/channels/thumbnail/";

let mongoose;

async function tryAsync(callback) {
  try {
    let data = await callback();
    return { error: undefined, data };
  } catch (error) {
    return { error, data: undefined };
  }
}

async function streamToString(stream) {
  return await new Promise((resolve, reject) => {
    const chunks = [];
    stream.on("data", (chunk) => chunks.push(chunk));
    stream.on("error", reject);

    stream.on("end", () => resolve(Buffer.concat(chunks))); //for fileCommand
  });
}

const s3Client = new S3Client({
  region: "us-west-2",
});

async function getSQLFile() {
  const fileCommand = new GetObjectCommand({
    Bucket: "onstream-channel-logos",
    Key: "logos.db",
  });

  const { Body: fileBody } = await s3Client.send(fileCommand);
  return await streamToString(fileBody);
}

function startSQlDb() {
  return new Database(FILE_NAME, { fileMustExist: true });
}

async function queryDb(db, suid) {
  let where = suid ? `where suid = ${suid}` : undefined;
  return await db
    .prepare(`SELECT suid, logo_id, logo_type from channellogo ${where}`)
    .all();
}

async function updateMongoDb(suid, data) {
  const ChannelLogos = mongoose.model("channelLogos");

  await ChannelLogos.findOneAndUpdate({ suid }, data, {
    new: true,
    upsert: true,
  });
}

function saveSQLFile(data) {
  fs.writeFileSync(FILE_NAME, data);
}

function deleteSQLFile() {
  fs.unlinkSync(FILE_NAME);
}

// connectMongo().then((data) => {
//   mongoose = data;
//   const db = startSQlDb();
//   console.log("db: ", db);
//   2;
//   queryDb(db, "9424")
//     .then((data) => {
//       console.log("dbQuery: ", data.data);
//       return data.data[0];
//     })
//     .then((data) => {
//       console.log("data: ", data);
//       updateMongoDb(9424, {
//         logo_url: `${BASE_URL}${0}.png`,
//       }).then((data) => console.log("update mongo: ", data));
//     });
// });

async function getChannelLogo(suid) {
  mongoose = await connectMongo();

  let file = await getSQLFile();
  saveSQLFile(file);

  let db = startSQlDb();
  let query = await queryDb(db, suid);
  console.log("Q", query);
  let logo_url = `${BASE_URL}${query[0].logo_id}.png`;

  await updateMongoDb(suid, { logo_url });

  deleteSQLFile();

  return logo_url;
}

async function syncFileToMongo() {
  mongoose = await connectMongo();

  let file = await getSQLFile();
  saveSQLFile(file);

  let db = startSQlDb();
  let query = await queryDb(db);
  let callbacks = query.map((obj) => {
    let { suid, logo_id } = obj;
    let logo_url = `${BASE_URL}${logo_id}.png`;
    return updateMongoDb(suid, { logo_url });
  });

  await Promise.all(callbacks);
}

syncFileToMongo().then(() => console.log("ran"));
// getChannelLogo(9424).then(() => console.log("ran"));
