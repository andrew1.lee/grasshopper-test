require("dotenv").config({ path: ".env" });
const mongoose = require("mongoose");

console.log("connecting to db: ", process.env.MONGO);

const connectMongo = async () => {
  await mongoose.connect(process.env.MONGO, {
    useNewUrlParser: true,
    // useFindAndModify: false,
    // useCreateIndex: true,
    // bufferMaxEntries: 0,
    bufferCommands: false,
    socketTimeoutMS: 2000000,
    useUnifiedTopology: true,
    maxPoolSize: 50,
    // poolSize: 50,
  });
  // CONNECTION EVENTS
  // When successfully connected
  mongoose.connection.on("connected", function () {
    console.log("Mongoose default connection open to " + process.env.MONGO);
  });

  // If the connection throws an error
  mongoose.connection.on("error", function (err) {
    console.log("Mongoose default connection error: " + err);
  });

  // When the connection is disconnected
  mongoose.connection.on("disconnected", function () {
    console.log("Mongoose default connection disconnected");
  });

  // If the Node process ends, close the Mongoose connection
  process.on("SIGINT", function () {
    mongoose.connection.close(function () {
      console.log(
        "Mongoose default connection disconnected through app termination"
      );
      process.exit(0);
    });
  });

  const channelLogosSchema = new mongoose.Schema(
    {
      suid: {
        type: Number,
        default: null,
        index: true,
        unique: true,
      },
      callsign: {
        type: String,
        default: null,
      },
      is_hd: {
        type: Number,
        default: null,
      },
      service_type: {
        type: String,
        default: null,
      },
      service_id: {
        type: Number,
        default: null,
      },
      logo_url: {
        type: String,
        default: null,
      },
      alias: {
        type: String,
        default: null,
      },
      spoken_name: {
        type: String,
        default: null,
      },
    },
    { collection: "channelLogos", versionKey: false }
  );

  mongoose.model("channelLogos", channelLogosSchema, "channelLogos");

  return mongoose;
};

module.exports = connectMongo;

// require("./models/user");
// require("./models/error");
// require("./models/parentalControls");
// require("./models/search");
// require("./models/favoriteseries");
// require("./models/favoritechannel");
// require("./models/watchLaterEpisode");
// require("./models/watchLaterEpisodeUserLink");
// require("./models/watchLaterItem");
// require("./models/watchLaterItemUserLink");
// require("./models/watchLaterRecording");
// require("./models/userSettings");
// require("./models/channelGenres");
// require("./models/channelGenresMappings");
// require("./models/channelsFoundWithoutGenres");

// require('dotenv').config({ path: '../../.env' })
// const mongoose = require('mongoose')
// const Models = require('./models')
// const promiseRetry = require('promise-retry')

// let conn = null

// /**
//  * Get database connection from cache or connect
//  */
// const getConnection = async () => {
//   return await promiseRetry(
//     async (retry) => {
//       if (conn && conn.readyState === 1) {
//         return {
//           conn,
//           Models: Models(conn),
//         }
//       }

//       try {
//         conn = await mongoose.createConnection(process.env.MONGO, {
//           useNewUrlParser: true,
//           useFindAndModify: false,
//           useCreateIndex: true,
//           bufferMaxEntries: 0,
//           bufferCommands: false,
//           socketTimeoutMS: 2000000,
//           useUnifiedTopology: true,
//         })
//         return {
//           conn,
//           Models: Models(conn),
//         }
//       } catch (e) {
//         console.log(e)
//         return e
//       }
//     },
//     { retries: 2 }
//   )
// }

// module.exports = getConnection
